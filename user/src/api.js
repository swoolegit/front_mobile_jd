import axios from 'axios';
import GlobalApp from 'GlobalApp'

const apiRequest = axios.create({
	baseURL:  GlobalApp.config.ApiUrl,
	headers: {
		"Content-Type": "application/json"
	},
});

export { apiRequest }

//export const apiBulletin = data => apiRequest.post('/api/bulletin/' + GlobalApp.config.company, data);
export const apiBulletin = () => apiRequest.get('/api/bulletin/' + GlobalApp.config.company);
export const apiMail = () => apiRequest.get('/api/mail');
export const apiMailRead = (id) => apiRequest.get('/api/mailread/' + id);


export const apiStartGame = (source, id) => apiRequest.get('/api/' + source + '/start-game/' + id);
export const apiTryGame = (source, id) => apiRequest.get('/api/' + source + '/try-game/' + id);

//取得平台餘額
export const apiGetBalance = (vendor) => apiRequest.get('/api/get-balance/' + vendor);
export const apiGetPayRule = () => apiRequest.get('/api/payrule/');

export const apiSendCode = data => apiRequest.post('/api/sms', data);
export const apiRegister = data => apiRequest.post('/api/register', data);
export const apiRegisterQuick = data => apiRequest.post('/api/register-quick', data);
export const apiLogin = data => apiRequest.post('/api/login', data);


export const apiChangePwd = data => apiRequest.post('/api/change-pwd', data);

export const apiSetupCashPassword = data => apiRequest.post('/api/setup-cashpassword', data);
export const apiResetCashPassword = data => apiRequest.post('/api/reset-cashpassword', data);
export const apiSetupEmail = data => apiRequest.post('/api/setup-email', data);
export const apiSmsAuth = data => apiRequest.post('/api/sms-auth', data);
export const apiVerifyMobile = data => apiRequest.post('/api/verify-mobile', data);
export const apiSetBank = data => apiRequest.post('/api/set-bank', data);

export const apiChangeName = data => apiRequest.post('/api/change-name', data);
export const apiChangeLine = data => apiRequest.post('/api/change-line', data);

export const apiCheckPaymentOrderNo = data => apiRequest.post('/api/check-payment-order-no', data);


export const apiTransferLog = (page, data) => apiRequest.post('/api/transfer-log?page=' + page, data);
export const apiBlog = (page, data) => apiRequest.get('/api/blog/'+ GlobalApp.config.company+'/?page=' + page, data);
export const apiBetLog = (page, data) => apiRequest.post('/api/bet-log?page=' + page, data);



export const apiCash = data => apiRequest.post('/api/cash', data);


export const apiForgetCode = data => apiRequest.post('/api/forget-code', data);
export const apiForgetVerifyCode = data => apiRequest.post('/api/forget-verify-code', data);
export const apiForgetResetPassword = data => apiRequest.post('/api/forget-reset-password', data);

export const apiPaymentBankAtm = data => apiRequest.post('/api/payment/bankatm', data);
export const apiPaymentVIPBank = data => apiRequest.post('/api/payment/vipbank', data);
export const apiPaymentHtpay = data => apiRequest.post('/api/payment/htpay', data);
export const apiPaymentEcpay = data => apiRequest.post('/api/payment/ecpay', data);
export const apiPaymentNewebPay = data => apiRequest.post('/api/payment/newebpay', data);
export const apiPaymentfafago = data => apiRequest.post('/api/payment/fafago', data);
export const apiPaymentpay8957 = data => apiRequest.post('/api/payment/pay8957', data);
export const apiPaymentleepay = data => apiRequest.post('/api/payment/leepay', data);
export const apiPaymentfunpay = data => apiRequest.post('/api/payment/funpay', data);

export const apiLogout = data => apiRequest.post('/api/logout', data);

/*
demo
export const apiUserLogin = data => userRequest.post('/signIn', data);
export const apiUserLogout = data => userRequest.post('/signOut', data);
export const apiUserSignUp = data => userRequest.post('/signUp', data);

export const apiArticleItem = () => articleRequest.get('/ArticleItem');
export const apiArticleMsg = data => articleRequest.post('/ArticleMsg', data);
export const apiArticleLink = data => articleRequest.post('/ArticleLink', data);

export const apiSearch = data => searchRequest.get(`/Search?searchdata=${data}`);
export const apiSearchType = () => searchRequest.get(`/SearchType`);*/
