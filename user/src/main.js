import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import Vuex from 'vuex';
Vue.use(Vuex);

import VueAwesomeSwiper from 'vue-awesome-swiper';

// require styles
import 'swiper/dist/css/swiper.css';
Vue.use(VueAwesomeSwiper);

import store from './store'


import axios from 'axios';
Vue.prototype.$axios = axios;

import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload,{
	preLoad: 1.3,
	attempt: 1,
})

Vue.use(require('vue-moment'));



//import {app} from 'app.js';
//Vue.use(app);

//import {appMixin} from 'appMixin.js'
//Vue.mixin(appMixin);
//import {app} from 'app.js';
//const store = new Vuex.Store(app);

//Vue.prototype.axios.defaults.baseURL = app.state.apiUrl;

import YDUI from 'vue-ydui'; /* 相当于import YDUI from 'vue-ydui/ydui.rem.js' */
/* 使用px：import 'vue-ydui/dist/ydui.px.css'; */

Vue.use(YDUI);

import vCascade from 'v-cascade'

Vue.use(vCascade)

import VuePreview from 'vue-preview'


import DefaultLayout from './components/layouts/Default.vue';
Vue.use(VuePreview,{
	mainClass: 'pswp--minimal--dark',
	barsSize: {top: 0, bottom: 0},
	captionEl: false,
	fullscreenEl: false,
	shareEl: false,
	bgOpacity: 0.85,
	tapToClose: true,
	tapToToggleControls: false
})

// Routes
const routes = [{
	path: '/',
	name: 'DefaultLayout',
	component: DefaultLayout,
		children: [{
				path: '',
			    name: '首頁',
			    component: () => import('./components/views/Home')
			},
			{
			    path: 'games/:id?',
			    name: '遊戲列表',
			    component: () => import('./components/views/Games')
			},
			{
			    path: 'av/login',
			    name: '遊戲列表',
			    component: () => import('./components/views/Games')
			},

			{
				path: 'user',
			    name: '會員中心',
			    component: () => import('./components/views/user/UserCenter')
			},
		]
	},


	{
		path: '/user/info',
		name: '個人設置',
		component: () => import('./components/views/user/Info')
	},
	{
		path: '/user/transfer-log',
		name: '資金記錄',
		component: () => import('./components/views/user/TransferLog')
	},
	{
		path: '/user/bet-log',
		name: '投注記錄',
		component: () => import('./components/views/user/BetLog')
	},
	/*
	{
		path: '/user/change-info',
		name: '設置個人資料',
		component: () => import('./components/views/user/ChangeInfo')
	},
	*/
	{
		path: '/user/change-name',
		name: '修改真實姓名',
		component: () => import('./components/views/user/ChangeName')
	},
	{
		path: '/user/change-line',
		name: '修改LINE',
		component: () => import('./components/views/user/ChangeLine')
	},
	{
		path: '/help/vip',
		name: '會員制度',
		component: () => import('./components/views/help/Vip')
	},
	{
		path: '/user/safe',
		name: '安全設置',
		component: () => import('./components/views/user/Safe')
	},
	{
		path: '/user/password',
		name: '修改登錄密碼',
		component: () => import('./components/views/user/Password')
	},
	{
		path: '/user/cash-password',
		name: '設置提款密碼',
		component: () => import('./components/views/user/CashPassword')
	},
	{
		path: '/user/reset-cashpassword',
		name: '重置提款密碼',
		component: () => import('./components/views/user/ResetCashpassword')
	},
	{
		path: '/user/email',
		name: '設定信箱',
		component: () => import('./components/views/user/Email')
	},
	{
		path: '/user/mail',
		name: '站內消息',
		component: () => import('./components/views/user/Mail')
	},
	{
		path: '/user/mobile',
		name: '手機認證',
		component: () => import('./components/views/user/Mobile')
	},
	{
		path: '/user/set-bank',
		name: '設定銀行',
		component: () => import('./components/views/user/SetBank')
	},
	{
		path: '/register',
	    name: '會員註冊',
	    component: () => import('./components/views/Register')
	},
	/*
	{
		path: '/register',
		name: '會員註冊',
		component: () => import('./components/views/RegisterQuick')
	},
	*/
	{
		path: '/login',
	    name: '會員登錄',
	    component: () => import('./components/views/Login')
	},
	{
		path: '/forget',
		name: '忘記密碼',
		component: () => import('./components/views/Forget')
	},
	{
		path: '/pricing',
	    name: '平台包網',
	    component: () => import('./components/views/Pricing')
	},
	{
		path: '/blog/:id?',
	    name: '文章分享',
	    component: () => import('./components/views/Blog')
	},
	{
		path: '/promotion/:id?',
	    name: '優惠活動',
	    component: () => import('./components/views/Promotion')
	},

	{
		path: '/promotion/Detail/:id',
	    name: '優惠活動內頁',
	    component: () => import('./components/views/PromotionDetail')
	},

	{
		path: '/games/slot/qt',
		name: 'QT電子遊戲',
		component: () => import('./components/views/SlotQT')
    },
    {
        path: '/games/slot/qt2',
        name: 'QT虛擬體育',
        component: () => import('./components/views/SlotQT2')
    },
	{
		path: '/games/slot/ae',
		name: 'AE電子遊戲',
		component: () => import('./components/views/SlotAE')
	},
	{
		path: '/games/slot/sa',
		name: 'SA電子遊戲',
		component: () => import('./components/views/SlotSA')
	},
	{
		path: '/games/slot/ps',
		name: 'PS電子遊戲',
		component: () => import('./components/views/SlotPS')
	},
	{
		path: '/games/slot/ttg',
		name: 'TTG電子遊戲',
		component: () => import('./components/views/SlotTTG')
	},
	{
	  path: '/games/slot/rtg',
	  name: 'RTG電子遊戲',
	  component: () => import('./components/views/SlotRTG')
	},
	{
		path: '/user/transfer/:vendor?',
	    name: '平台轉帳',
	    component: () => import('./components/views/user/Transfer')
	},
	{
		path: '/user/game/transfer/:vendor?/:id?',
	    name: '遊戲轉點',
	    component: () => import('./components/views/user/GameTransfer')
	},
	{
		path: '/user/wallet',
	    name: '資產總覽',
	    component: () => import('./components/views/user/Wallet')
	},
	{
		path: '/user/deposit',
	    name: '存款',
	    component: () => import('./components/views/user/Deposit')
	},
	{
		path: '/user/withdraw',
	    name: '提款',
	    component: () => import('./components/views/user/Withdraw')
	},
	{
	  path: '/help',
	  name: '幫助中心',
	  component: () => import('./components/views/help/Help')
	},
	{
	  path: '/help/hot/:id/question',
	  name: '熱門問題',
	  component: () => import('./components/views/help/HotQuestion')
	},
	{
	  path: '/help/category/:id',
	  name: '問題分類',
	  component: () => import('./components/views/help/Category')
	},
	{
	  path: '/help/category/:id/content',
	  name: '問題詳情',
	  component: () => import('./components/views/help/CategoryContent')
	},


/*
	{
	  path: '/help',
	  name: '幫助中心',
	  component: () => import('./components/views/Help')
	},

	{
	  path: '/helpCategery/:cid?',
	  name: '',
	  component: () => import('./components/views/HelpCategery')
	},

	{
	  path: '/helpDetail/:cid?/:id?',
	  name: '問題詳情',
	  component: () => import('./components/views/HelpDetail')
	},

*/
];


import Vonic from 'vonic';


//todo 3.设置全局路由钩子
import sess from './sess';
//console.log(Vonic.app.dio);
Vonic.app.setConfig(
	'beforeEach',
	function(toRoute, fromRoute, next) {
//console.log(toRoute);
//console.log(fromRoute);
		const to = toRoute.path;
		const from = fromRoute.path;
		const scrollTop = Vonic.app.pageContentScrollTop();
		//console.log(Vonic.app);
//		if (to == '/user/center') {
			//console.log(next('/login'));
//		}

		let h = sess.get(to)
			//console.log('before',to,h,scrollTop);
			//console.log(from,to);
	//	console.log(to,h);
		if (h && h.history) {
			//vonic bug 當用route replace時方向會錯亂

			//if ((from === '/games/live' && to.indexOf('/promotion') != -1)
			//	|| (from === '/user/' && to.indexOf('/promotion') != -1)
			//	|| (from === '/' && to.indexOf('/promotion') != -1)
			//	|| (from.indexOf('/games/') != -1 && to.indexOf('/promotion') != -1)
			//) {
			//	Vonic.app.nextDirection('forward')
			//}
			//else {
				Vonic.app.nextDirection('back')

			//}
			h.history = false
			sess.set(to, h)
			h.scrollTop = scrollTop
			sess.set(from, h)
		} else {
			sess.set(from || '/', {
				history: true,
				scrollTop: scrollTop
			})
			//if ((to === '/games/live' && from.indexOf('/promotion') != -1 )
			//	|| (to === '/user/' && from.indexOf('/promotion') != -1)
			//	|| (to === '/' && from.indexOf('/promotion') != -1)
			//	|| (to.indexOf('/games/') != -1 && from.indexOf('/promotion') != -1)
			//) {
			//console.log('forward');
			//	Vonic.app.nextDirection('back')
			//}
			//else {
				Vonic.app.nextDirection('forward')

			//}

//			Vonic.app.nextDirection('forward')
		}
		axios.get(store.getters.config.ApiUrl + 'static/version.json', {})
			.then(response => {
				if (store.getters.version !== 0 && store.getters.version !== undefined){
					if(store.getters.version !== response.data.version) {
						location.reload();
					}
				}
				store.getters.version = response.data.version;
			}).catch(error => {
			})
		console.log(to);
		if (to === "/login" || to === "/register" || to==="/forget") {
			next();
		}
		else {
			if (!store.getters.user.isLogin){
				return next({path: "/login"});
			}else {
				next();
			}
		}
/*		
		var regex = /^\/user\//i;
		if (to.search(regex) === -1) {
			next();
		}
		else {
			if (!store.getters.user.isLogin){
				axios.get(store.getters.config.ApiUrl + '/api/check-login', {
					params: {
					}
				}).then(response => {
					//console.log(response.data.status);
					if(response.data.status) {
						store.getters.user.isLogin = true;
						next();
					}
					else {
						return next({path: "/"});
					}
					//console.log(response.data);
				}).catch(error => {
					console.log(error);
					return next({path: "/"});

				})
			}
			else {
				next();
			}

		}
*/
		//next();
	}
);
Vonic.app.setConfig(
	'afterEach',
	function(toRoute, fromRoute) {
		const to = toRoute.path
		const from = fromRoute.path

		if (to == '/') return

		const h = sess.get(to)
		//console.log(h);
		if (h && h.scrollTop) {
		  Vue.nextTick(() => {
		    Vonic.app.pageContentScrollTop(h.scrollTop)
		  })
		}
	}
);
Vonic.app.setConfig( //定义页面切换样式（push/replace），默认为push，不推荐replace
	'pushMethod', 'push'
);
Vue.use(Vonic.app, {
	routes:routes,
	store:store,
});
var vm = new Vue({store});
vm.$store.dispatch('checkLogin');

import 'vue-ydui/dist/ydui.base.css';
import 'vue-ydui/dist/ydui.px.css';
import './assets/style.css'
