import { apiRequest, apiTryGame, apiStartGame,apiLogout } from 'api.js'
export const globalMixin = {
	methods: {
		currencyBox(){
			let options = {
				effect: 'scale',
				title: '幣值說明',
				showClose: false,
				buttons: [
				  {text: '我知道了'},
				]
			}
			let popup = $popup.fromTemplate('<div class="fs14 text-left">遊戲幣值比例5:1，轉換範例如下:<br><span style="color:#ff5722">主錢包轉出10元 = VG真人轉入2元</span><br><br>遊戲幣值比例1:5，轉換範例如下:<br><span style="color:#ff5722">VG真人轉出2元 = 主錢包轉入10元</span></div>', options);
			popup.show().then((buttonIndex) => {});
		
		},

		focus(target){
			this.$refs[target].focus();
		},
		logout(){
			let options = {
				effect: 'scale',
				title: '提示',
				showClose: false,
				buttons: [
				  {text: '取消'},
				  {text: '確定'},
				]
			}
			let popup = $popup.fromTemplate('<div class="fs14 text-center">是否登出會員?</div>', options);

			popup.show().then((buttonIndex) => {
				if(buttonIndex == 1) {
					this.$dialog.loading.open('登出中');
					setTimeout(() => {
						apiLogout()
						.then(response => {
							this.$dialog.loading.close();
							if(response.data.status) {

								this.web.tabbarMenus[2].iconOn = 'ion-ios-paper-outline';
								this.web.tabbarMenus[2].iconOff = 'ion-ios-paper-outline';
								this.web.tabbarMenus[2].path = '/register';
								this.web.tabbarMenus[2].text = '註冊';

								this.web.tabbarMenus[4].path = '/login';
								this.web.tabbarMenus[4].text = '登錄';

								this.user.isLogin = false;
								this.user.wallet.refresh = false;
								this.user.wallet.totalAmounts = 0;
								if (/^\/user/.test(this.$router.currentRoute.path)){
									this.$router.replace('/');
								}

							}
							this.$dialog.notify({
								mes: response.data.message,
								timeout: 4000,
								callback: () => {
								}
							});
							//$toast.show(response.data.message);
						})
						.catch(error => {
							console.log(error);
						});
					
					}, 400);
				}
	
			})

		},
		/*
		logout(){
			$dialog.confirm({
				content: '是否登出會員?',
				okText: '確定',
				okTheme:'positive',
				cancelText: '取消',
			}).then((res) => {
				if (!res) return false;
				apiLogout()
					.then(response => {
						if(response.data.status) {
							this.web.tabbarMenus[2].iconOn = 'ion-ios-paper-outline';
							this.web.tabbarMenus[2].iconOff = 'ion-ios-paper-outline';
							this.web.tabbarMenus[2].path = '/register';
							this.web.tabbarMenus[2].text = '註冊';

							this.web.tabbarMenus[4].path = '/login';
							this.web.tabbarMenus[4].text = '登錄';

							this.user.isLogin = false;
							this.user.wallet.refresh = false;
							this.user.wallet.totalAmounts = 0;
							if (/^\/user/.test(this.$router.currentRoute.path)){
								this.$router.replace('/');
							}

						}
						$toast.show(response.data.message);
					})
					.catch(error => {
						console.log(error);
					});


			});
		
		},*/
		go2page(url){
			this.$router.push({path: url, query: {}});
		},
		isIOS() {
			return !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
		},

		startGame(source, id, string) {
			if(this.user.isLogin == false) {
				this.$router.push({path: '/login', query: {}});
				return false;
			}
			this.go2page('/user/game/transfer/' + source + '/' + id);
			
		},
		tryGame(source, id, string) {
			$loading.show(string ? string : '遊戲啟動中');
			if(this.isIOS()) {
				var newWindow = window.open("about:blank", '');
			}
			apiTryGame(source, id)
				.then(response => {
					if (!response.data.status) {
						return $toast.show(response.data.message);
					}
					else {
						if(this.isIOS()) {
							newWindow.location.href = response.data.data.url;
						}
						else {
							window.open(response.data.data.url,'', '');
						}
						
					}
					$loading.hide();
				})
				.catch(error => {
					console.log(error);
					$loading.hide();
				});

		},
		onlyNumber: function(event) {
			event = (event) ? event : window.event;
			var charCode = (event.which) ? event.which : event.keyCode;
			//if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
			if ((charCode > 31 && (charCode < 48 || charCode > 57)) || charCode == 46) {
				event.preventDefault();;
			} else {
				return true;
			}
		},
	},
	created(){
							
		apiRequest.interceptors.response.use((response) => {


			if(response.data.status === 404 && !this.$store.state.user.error) {
				this.$store.state.user.error = true;
				setTimeout(() => {
						this.$dialog.loading.close();
						if (this.$refs.scroller) this.$refs.scroller.finishPullToRefresh();
				}, 400);

				$dialog.alert({
					//okTheme: 'positive',
					theme: 'ios',
					effect:  'scale',
					title: '連線已失效',
					content: '目前您的連線已失效，請重新登錄',
					okText: '確定'
				}).then(() => {
					//var a = document.createElement("a");
					//a.setAttribute("href", "/");
					//a.click();
					//if(navigator.userAgent.match(/Android/i)) {
						//document.location = '/#' + this.$router.currentRoute.path;      
				//		document.location = '/';
				//	}
				//	else{
				//		alert(1);
					//	window.open('/', '_self', false);
						//window.location.replace('/');
				//	}			
					this.$store.state.user.error = false;
					this.$router.push('/login');

				})
				this.$store.state.web.tabbarMenus[2].iconOn = 'ion-ios-paper-outline';
				this.$store.state.web.tabbarMenus[2].iconOff = 'ion-ios-paper-outline';
				this.$store.state.web.tabbarMenus[2].path = '/register';
				this.$store.state.web.tabbarMenus[2].text = '註冊';

				this.$store.state.web.tabbarMenus[4].path = '/login';
				this.$store.state.web.tabbarMenus[4].text = '登錄';

				this.$store.state.user.isLogin = false;
				this.$store.state.user.wallet.refresh = false;
				this.$store.state.user.wallet.totalAmounts = 0;

				return response;
//				$toast.show('請先登錄會員!');
				//this.$store.state.web.tabbarMenus[2].iconOn
				//console.log(this.$store.state);
				/*
				this.$store.state.web.tabbarMenus[2].iconOn = 'ion-ios-paper-outline';
				this.$store.state.web.tabbarMenus[2].iconOff = 'ion-ios-paper-outline';
				this.$store.state.web.tabbarMenus[2].path = '/register';
				this.$store.state.web.tabbarMenus[2].text = '註冊';

				this.$store.state.web.tabbarMenus[4].path = '/login';
				this.$store.state.web.tabbarMenus[4].text = '登錄';

				this.$store.state.user.isLogin = false;
				this.$store.state.user.wallet.refresh = false;
				this.$store.state.user.wallet.totalAmounts = 0;
				*/

				if (/^\/user/.test(this.$router.currentRoute.path)){
					//this.$router.replace('/');
					this.$router.push('/login')
				}

	
				return response;

				
			}
			else {
				return response;
			}
		},(error) => {
			setTimeout(() => {
					this.$dialog.loading.close();
					if (this.$refs.scroller) this.$refs.scroller.finishPullToRefresh();
			}, 400);		

			if ((error.response.status == 429 || error.response.status == 503) && !this.$store.state.user.error){
				this.$store.state.user.error = true;

				$dialog.alert({
					//okTheme: 'positive',
					theme: 'ios',
					effect:  'scale',
					title: '偵測異常',
					content: '操作過於頻繁，請稍候再試!',
					okText: '確定'
				}).then(() => {
					this.$store.state.user.error = false;
					//this.$router.push('/login');
				})
			
			}

		});


		
	}
}