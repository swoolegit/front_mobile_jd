import axios from 'axios'
//import router from 'vue-router'

export default {
	//addCount({ commit }) {
	//	commit('addCount', 1) //呼叫mutations
	//},
	getSlotLists ({ commit }, vendor) {
		//console.log(vendor);

		axios.get(this.state.config.ApiUrl + '/api/slot/'+vendor, {
			params: {
			}
		}).then(response => {

			this.state.web[vendor] = response.data;
			//console.log(this.state.web);
			//commit('actionAJAXexample', response.data);
		}).catch(error => {
			console.log(error);
		})
	},
	//檢查是否登錄狀態
	checkLogin ({ commit, dispatch }) {
		axios.get(this.state.config.ApiUrl + '/api/check-login', {
			params: {
			}
		}).then(response => {
			if (response.data.status){

				dispatch('getUserInfo');



			}

		}).catch(error => {
			console.log(error);
		})
	},
	//同步檢查是否登錄狀態
	async asyncCheckLogin ({ commit }) {
		let result = await axios.get(this.state.config.ApiUrl + '/api/check-login', {params: {}});
		result = result.data;
		try {
			return new Promise((resolve) => {
				resolve(result);
			});
		} catch (err) {
			console.log(err)
		}

	},
	//檢查是否登錄狀態
	getUserInfo ({ commit }) {
		axios.get(this.state.config.ApiUrl + '/api/get-user-info', {
			params: {
			}
		}).then(response => {

			let data = response.data.data;
			this.state.user.isLogin = response.data.status;
			if (this.state.user.wallet.deploy['WALLET']) {
				this.state.user.wallet.deploy['WALLET'].amount = parseFloat(data.wallet);
			}
			else {
				this.state.user.wallet.deploy = {'WALLET' : {'amount' : parseFloat(data.wallet) }};

			}
			this.state.user.username = data.username;
			this.state.user.name = data.name;
			this.state.user.class = data.class;
			this.state.user.login_time = data.login_time;
			this.state.user.mobile = data.mobile;
			this.state.user.email = data.email;
			this.state.user.is_cash_password = data.is_cash_password;
			this.state.user.created_at = data.created_at;
            this.state.user.line = data.line;
			this.state.user.unread = data.unread;
			this.state.user.bank_atm = data.bank_atm;
			this.state.user.vip_bank = data.vip_bank;
			if (!data.bank)	{
				this.state.user.bank = {'bank_value':'','bank_name':'','bank_account':'','bank_username':''};
			}
			else {
				this.state.user.bank = data.bank;
			}

            this.state.user.isShowAppDL = true;

			this.state.web.tabbarMenus[2].iconOn = 'ion-cash';
			this.state.web.tabbarMenus[2].iconOff = 'ion-cash';
			this.state.web.tabbarMenus[2].path = '/user/deposit';
			this.state.web.tabbarMenus[2].text = '買入';

			this.state.web.tabbarMenus[4].path = '/user/';
			this.state.web.tabbarMenus[4].text = '我的';

			//console.log(response.data);
		}).catch(error => {
			console.log(error);
		})
	},
	upIsLogin ({ commit },status) {
		this.state.user.isLogin = status;
	}


}
