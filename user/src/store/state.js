import GlobalApp from 'GlobalApp'
export default {
	version: 0,
	config : GlobalApp.config,
	user:{
		uid : null,
		username : null,
		name : null,
		isLogin : false,
		currentRoute : '',
		vendorsCount : 0,
		wallet :{
			setbank:false,
			totalAmounts:0,
			sync:'',
			refresh : false,
			deploy :{},
		},
		class : 0,
		login_time : '',
		mobile : '',
		email : '',
		bank : {
			bank_name:'',
			bank_value:'',
			bank_account:'',
			bank_username:'',
		},
		is_cash_password : false,
		created_at : '',
        line: '',
        unread: 0,
        isShowAppDL: true,
		error:false,
		bank_atm:'',

	},
	web:{
		temp:{},
		bulletins:[],
		qt:[],
		ae: [],
		sa_slot: [],
		//ddfg: [],
		//dg: [],
		ps: [],
		//wm: [],
		ttg: [],
		//og: [],
		rtg: [],

		banklists: [],

		tabbarMenus: [{
                iconOn: 'ion-ios-home',
                iconOff: 'ion-ios-home-outline',
                text: '首頁',
                path: '/'
            },
            {
                iconOn: 'ion-ios-keypad',
                iconOff: 'ion-ios-keypad-outline',
                text: '設置',
                path: '/user/safe'
            },
            {
                iconOn: 'ion-ios-paper-outline',
                iconOff: 'ion-ios-paper-outline',
                text: '註冊',
                path: '/register',
            },
            {
				iconOn: 'ion-ios-chatboxes',
				iconOff: 'ion-ios-chatboxes-outline',
				text: '客服',
				//path: '/service',
            },
            {
				iconOn: 'ion-ios-contact',
				iconOff: 'ion-ios-contact-outline',
				text: '登錄',
				path: '/login'
				//  badge: '5'
            }
        ],


	},
	datalist: [],

}
